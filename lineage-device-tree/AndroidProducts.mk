#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_2120.mk

COMMON_LUNCH_CHOICES := \
    lineage_2120-user \
    lineage_2120-userdebug \
    lineage_2120-eng
