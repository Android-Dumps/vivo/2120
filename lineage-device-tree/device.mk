#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    install-recovery.sh \
    init.vivo.fingerprint.sh \
    collect_connsys_dump.sh \
    zramsize_reconfig.sh \
    init.insmod.sh \
    init.vivo.fingerprint_restart_counter.sh \
    init.vivo.crashdata.sh \

PRODUCT_PACKAGES += \
    fstab.mt6765 \
    init.sensor_1_0.rc \
    factory_init.rc \
    init.project.rc \
    init.connectivity.rc \
    factory_init.project.rc \
    init_connectivity.rc \
    meta_init.connectivity.common.rc \
    multi_init.rc \
    meta_init.modem.rc \
    init.mt6765.usb.rc \
    meta_init.rc \
    init.factory.rc \
    init.mt6765.rc \
    init.modem.rc \
    factory_init.connectivity.rc \
    factory_init.connectivity.common.rc \
    init.cgroup.rc \
    init.aee.rc \
    meta_init.connectivity.rc \
    init.ago.rc \
    init.connectivity.common.rc \
    meta_init.project.rc \
    init.recovery.mt6765.rc \
    init.recovery.svc.rc \
    init.recovery.platform.rc \
    init.recovery.wifi.rc \
    init.recovery.touch.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.mt6765:$(TARGET_COPY_OUT_RAMDISK)/fstab.mt6765

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 30

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/vivo/2120/2120-vendor.mk)
